#
#  Be sure to run `pod spec lint MeetingLawyersNSE.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "MeetingLawyers"
  # Change version MeetingLawyers.release.xcconfig var
  spec.version      = "2.0.0-alpha-16"
  spec.summary      = "A MeetingLawyers SDK."
  spec.swift_versions = ['5.0']

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  spec.description  = <<-DESC
                    A MeetingLawyers SDK for chat and video call.
                    [SDK Doc](https://developer.meetinglawyers.com/)
                   DESC

  spec.homepage = 'https://www.meetinglawyers.com'

  spec.license = { :type => 'Copyright', :text => 'Copyright © 2022 MeetingLawyers S.L. All rights reserved.' }
  spec.source = { :http => "https://meetinglawyers-ios-sdk.s3.eu-west-3.amazonaws.com/new-sdk/#{spec.version}/MeetingLawyers_#{spec.version}.zip" }
  spec.author = { "MeetingLawyers" => "accounts@meetinglawyers.com" }
  
  spec.ios.deployment_target = '14.0'
  spec.ios.vendored_frameworks = 'MeetingLawyers/MeetingLawyers.xcframework'
  spec.ios.dependency 'Socket.IO-Client-Swift', '15.2.0'
  spec.ios.dependency 'TUSKit', '3.3'

end
